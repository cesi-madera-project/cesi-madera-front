import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import moment from 'moment'

Vue.use(Vuex)

// api connection
const urlconnection = 'http://localhost:3000/api/connexion';
const urlquote = 'http://localhost:3000/api/quote';
const urlconfiguration = 'http://localhost:3000/api/configuration';
const urlpayment = 'http://localhost:3000/api/payment/projet-paiement';

export default new Vuex.Store({
    state: {
        idUser: "",
        firstname: "",
        login: "",
        token: localStorage.getItem('user_token') || "",
        status: "",
        session: [],
        sessionId: "",
        error: "",
        AllQuoteUserList: [],
        CustomerNameList: [],
        ProgressQuoteUserList: [],
        ValidateQuoteUserList: [],
        ResearchQuoteUserList: [],
        Status: "",
        Customer: [],
        idCustomer: "",
        Project: [],
        idProject: "",
        Product: [],
        idProduct: [],
        ProductProject: [],
        Module: [],
        idModule: [],
        ModuleProject: [],
        Component: [],
        idComponent: [],
        ComponentProject: [],
        ComponentProduct: [],
        ComponentModule: [],
        paymentList: [],
        modelList: [],
        rangeList: [],
        familyComponentList: [],
        componentList: [],
        providerList: [],
        frameList: [],
        settingList: [],
        cptUserQuotesCreate: 0,
        cptUserQuotesValidate: 0,
        cptQuotesValidate: 0,
        cptUserQuotesRelaunch: 0,
        cptGoalDevis: 438,
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status,
        getModelList(state) {
            return state.modelList;
        },
        getRangeList(state) {
            return state.rangeList;
        },
        getFamilyComponentList(state) {
            return state.familyComponentList;
        },
        getComponentList(state) {
            return state.componentList;
        },
        getProviderList(state) {
            return state.providerList;
        },
        getFrameList(state) {
            return state.frameList;
        },
        getSettingList(state) {
            return state.settingList;
        },
        getPaymentList(state) {
            return state.paymentList;
        },
    },
    mutations: {
        SET_ALL_USER_QUOTES_LIST(state, AllQuoteUserList) {
            state.AllQuoteUserList = AllQuoteUserList;
        },
        SET_ALL_USER_PROGRESS_QUOTES_LIST(state, ProgressQuoteUserList) {
            //console.log('****devis******')
            //console.log(ProgressQuoteUserList[0])
            state.ProgressQuoteUserList = ProgressQuoteUserList;
        },
        SET_ALL_USER_VALIDATE_QUOTES_LIST(state, ValidateQuoteUserList) {
            state.ValidateQuoteUserList = ValidateQuoteUserList;
        },
        SET_PROJECT_BY_RESEARCH(state, ResearchQuoteUserList) {
            //console.log('ResearchQuoteUserList');
            //console.log(ResearchQuoteUserList);
            state.ResearchQuoteUserList = ResearchQuoteUserList;
        },
        SET_CUSTOMER(state, Customer) {
            state.Customer = Customer;
            state.idCustomer = Customer._id;
        },
        SET_CUSTOMERNAME(state, CustomerNameList) {
            state.CustomerNameList = CustomerNameList;
        },
        SET_QUOTEINFORMATION(state, Customer) {
            state.idCustomer = Customer._id;
            state.Customer = Customer;
        },
        SET_QUOTEPROJET(state, project) {
            state.Project = project;
            state.idProject = project._id;
        },
        SET_CREATEPDF() {
            console.log('pdf genere');
        },
        SET_QUOTEPROJETPRODUCT(state, productproject) {
            console.log("SET_QUOTEPROJETPRODUCT");
            state.ProductProject = [];
            state.idProduct = [];
            for (const key in productproject) {
                state.ProductProject.push(productproject[key]);
                state.idProduct.push(productproject[key].productid);
            }
        },
        SET_QUOTEPRODUCT(state, product) {
            console.log("SET_QUOTEPRODUCT");
            state.Product = [];
            for (const key in product) {
                state.Product.push(product[key]);
            }
        },
        SET_QUOTEPROJETMODULE(state, moduleproject) {
            console.log("SET_QUOTEPROJETMODULE");
            state.ModuleProject = [];
            state.idModule = [];
            //console.log(moduleproject);
            for (const key in moduleproject) {
                state.ModuleProject.push(moduleproject[key]);
                state.idModule.push(moduleproject[key].moduleid);
            }
        },
        SET_QUOTEMODULE(state, module) {
            console.log("SET_QUOTEMODULE");
            state.Module = [];
            //console.log(module);
            for (const key in module) {
                state.Module.push(module[key]);
            }
        },
        SET_QUOTEPROJETCOMPONENT(state, componentproject) {
            console.log("SET_QUOTEPROJETCOMPONENT");
            console.log('componentproject');
            console.log(componentproject)
            
            if(componentproject[0] != undefined){
                state.idComponent = [];
                state.Component = [];
                console.log("if");
                for (const key in componentproject) {
                    var errorCP = 0;
                    var errorC = 0;
                    for (const keyCP in state.ComponentProject) {
                        if(keyCP == componentproject[key]){
                            errorCP = 1;
                        }
                    }
                    for (const keyC in state.idComponent) {
                        if(keyC == componentproject[key].componentid){
                            errorC = 1;
                        }
                    }
                    if (errorCP == 0 && errorC == 0){
                        state.ComponentProject.push(componentproject[key]);
                        state.idComponent.push(componentproject[key].componentid);
                    }
                }
            }else{
                console.log("else");
                console.log("Composant");
                console.log(state.Component);
                console.log("Module");
                console.log(state.Module);
                console.log("Produit");
                console.log(state.Product);
                
                for (const key1 in state.Component){
                    if(componentproject._id == state.Component[key1]._id){
                        console.log("bon id projet");
                        if(state.Component[key1].name != componentproject.name){
                            state.Component[key1].name = componentproject.name;
                        }

                        for(const key2 in state.Module){
                            if(componentproject.module == state.Module[key2]._id){
                                console.log("bon id module");
                                state.Component[key1].module = state.Module[key2];
                            }
                        }

                        for (const key3 in state.Product){
                            if(componentproject.product == state.Product[key3]._id){
                                console.log("bon id product");
                                state.Component[key1].product = state.Product[key3];
                            }
                        }
                    }
                }
                console.log("Composant aprés");
                console.log(state.Component);
            }
        },
        SET_QUOTECOMPONENT(state, component) {
            console.log("SET_QUOTECOMPONENT");
            //console.log(component[0]);
            state.Component = [];
            for (const key in component) {
                state.Component.push(component[key]);
            }
        },
        SET_PAYMENT_LIST(state, paymentList) {
            state.paymentList = paymentList;
        },
        SET_MODEL_LIST(state, modelList) {
            state.modelList = modelList;
        },
        SET_RANGE_LIST(state, rangeList) {
            state.rangeList = rangeList;
        },
        SET_FAMILYCOMPONENT_LIST(state, familyComponentList) {
            state.familyComponentList = familyComponentList;
        },
        SET_COMPONENT_LIST(state, componentList) {
            state.componentList = componentList;
        },
        SET_PROVIDER_LIST(state, providerList) {
            state.providerList = providerList;
        },
        SET_FRAME_LIST(state, frameList) {
            state.frameList = frameList;
        },
        SET_SETTING_LIST(state, settingList) {
            state.settingList = settingList;
        },
        SET_CONNECTION(state, payload) {
            state.status = 'success'
            state.login = payload.login;
            state.session = payload.session;
            state.sessionId = payload.sessionId

            localStorage.setItem('idUser', payload.userId)
            localStorage.setItem('firstname', payload.firstName)
            localStorage.setItem('user_token', payload.token);
            state.idUser = localStorage.idUser;
            state.firstname = localStorage.firstname;
            state.token = localStorage.user_token;
        },
        AUTH_REQUEST: (state) => {
            state.status = 'loading'
        },
        AUTH_ERROR: (state, error) => {
            state.status = 'error'
            state.error = error;
        },
        AUTH_LOGOUT: (state) => {
            localStorage.removeItem('user_token');
            localStorage.removeItem('firstname');
            localStorage.removeItem('idUser');
            state.status = ''
            state.token = '';
            state.idUser = '';
            state.login = '';
            state.firstName = '';
            state.session = '';
            state.sessionId = ''
            state.token = localStorage.getItem('user_token') || "";
        },
        SET_CPT_USER_QUOTE(state) {
            state.cptUserQuotesCreate = 0;
            //console.log("calcul nombre de devis fait : " + state.quotesUserList.length);
            state.cptUserQuotesCreate = state.quotesUserList.length;
        },
        SET_CPT_USER_QUOTE_VALIDATE(state) {
            state.cptUserQuotesValidate = 0;
            for (const quoteUserList of state.quotesUserList) {
                if (quoteUserList.state._id == '5f1025b27b23f59e1bc924bd') {
                    state.cptUserQuotesValidate++;
                }
            }
            //console.log("calcul du nombre de devis validé par le commercial: " + state.cptUserQuotesValidate);
        },
        SET_CPT_QUOTE_VALIDATE(state) {
            //console.log("affiche le nombre de devis validé : " + state.quotesList.length);
            for (const quoteList of state.quotesList) {
                if (quoteList.state._id == '5f1025b27b23f59e1bc924bd') {
                    state.cptQuotesValidate++;
                }
            }
        },
        SET_CPT_USER_QUOTE_RELAUNCH(state) {
            state.cptUserQuotesRelaunch = 0;
            console.log("affiche le nombre de devis à relancer : " + state.quotesUserList.length);
            for (const quoteUserList of state.quotesUserList) {
                if (!quoteUserList.payment) {
                    if (quoteUserList.state._id == '5f1024c37b23f59e1bc924bc') {
                        state.cptUserQuotesRelaunch++;
                    }
                }
            }
        },
        DELETEPROJECT() {
            console.log('suppression effectuée');
        }
    },
    actions: {
        /*
        * PARTIE CONNEXION
        */
        async controlConnection({ commit }, payload) {
            //Déclaration variables
            let reqLogin = payload.login;
            let reqPassword = payload.password;
            return new Promise((resolve, reject) => {
                commit("AUTH_REQUEST")
                axios.post((urlconnection), { login: reqLogin, password: reqPassword })
                    .then((response) => {
                        axios.defaults.headers.common['Authorization'] = response.data.token;
                        commit("SET_CONNECTION", response.data);
                        resolve();
                    })
                    .catch((error) => {
                        commit("AUTH_ERROR", error.response.data.error);
                        localStorage.removeItem('user_token');
                        reject(error.response.data.error);
                    });
            })
        },
        auth_logout: ({ commit }) => {
            return new Promise((resolve) => {
                commit("AUTH_LOGOUT")
                // remove the axios default header
                delete axios.defaults.headers.common['Authorization']
                resolve();
            })
        },
        /*
        * PARTIE CONCEPTION DE DEVIS
        */
        async quoteResearch({ commit }, payload) {
            //Déclaration variables
            let ref = "";
            if (payload.ref) { ref = payload.ref }
            let phoneNumber = "";
            if (payload.phoneNumber) { phoneNumber = payload.phoneNumber }
            let name = "";
            if (payload.name) { name = payload.name }
            let email = "";
            if (payload.email) { email = payload.email }

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/research-informations-quote'), {
                    cli_ref: ref,
                    cli_phoneNumber: phoneNumber,
                    cli_name: name,
                    cli_email: email
                })
                    .then((response) => {
                        commit("SET_QUOTERESULTRESEARCH", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadAllUserQuotesList({ commit }) {
            //Déclaration des variables
            var idUser = this.state.idUser;
            var urluserquotes = urlquote + "/all-quotes-user/";

            return new Promise((resolve, reject) => {
                axios.post(urluserquotes, { idUser: idUser })
                    .then((response) => {
                        commit("SET_ALL_USER_QUOTES_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    })
                    )
            })
        },
        loadAllUserProgressQuotesList({ commit }) {
            //Déclaration des variables
            var idUser = this.state.idUser;
            var urluserquotes = urlquote + "/progress-quotes-user/";

            return new Promise((resolve, reject) => {
                axios.post(urluserquotes, { idUser: idUser })
                    .then((response) => {
                        commit("SET_ALL_USER_PROGRESS_QUOTES_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    })
                    )
            })
        },
        loadAllUserValidateQuotesList({ commit }) {
            //Déclaration des variables
            var idUser = this.state.idUser;
            var urluserquotes = urlquote + "/validate-quotes-user/";

            return new Promise((resolve, reject) => {
                axios.post(urluserquotes, { idUser: idUser })
                    .then((response) => {
                        commit("SET_ALL_USER_VALIDATE_QUOTES_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    })
                    )
            })
        },
        async quoteResearchQuoteByPhoneNumber({ commit }, payload) {
            let phoneNumber = payload.phoneNumber;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/research-project-by-phonenumber'), {
                    cli_phonenumber: phoneNumber,
                })
                    .then((response) => {
                        commit("SET_PROJECT_BY_RESEARCH", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async quoteResearchCustomer({ commit }, payload) {
            //Déclaration variables
            let phonenumber = payload.phoneNumber;
            let name = payload.name;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/research-customer'), {
                    cli_name: name,
                    cli_phonenumber: phonenumber,
                })
                    .then((response) => {
                        commit("SET_CUSTOMER", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async deleteProject({ commit }, payload) {
            //Déclaration variables
            let ref = payload.reference;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/delete-quote'), {
                    reference: ref,
                })
                    .then((response) => {
                        commit("DELETEPROJECT", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async getAllCustomerName({ commit }) {
            //async getAllCustomerName({ commit }, payload) {
            //Déclaration de variables
            //var nameSelected = payload.namePart;

            return new Promise((resolve, reject) => {
                /*axios.post(urlquote + '/research-customer-name-selected', {
                    namePart : nameSelected,
                })*/
                axios.post(urlquote + '/research-customer-name')
                    .then((response) => {
                        commit("SET_CUSTOMERNAME", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })

        },
        async getCustomer({ commit }) {
            //Déclaration variable
            let id = this.state.idCustomer;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-customer'), {
                    id: id,
                })
                    .then((response) => {
                        commit("SET_CUSTOMERQUOTE", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async loadQuoteProjectProductList({ commit }) {
            console.log('loadQuoteProjectProductList');
            //Déclaration variable
            let project_id = this.state.idProject;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-project-product'), {
                    idProject: project_id,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJETPRODUCT", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async loadQuoteProductList({ commit }) {
            console.log('loadQuoteProductList');
            //Déclaration variable
            let product_id = this.state.idProduct;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-product'), {
                    idProduct: product_id,
                })
                    .then((response) => {
                        commit("SET_QUOTEPRODUCT", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async loadQuoteProjectModuleList({ commit }) {
            console.log('loadQuoteProjectModuleList');
            //Déclaration variable
            let project_id = this.state.idProject;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-project-module'), {
                    idProject: project_id,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJETMODULE", response.data);
                        resolve();
                    })
                    .then((response) => {
                        commit("SET_QUOTEPROJETPRODUCT", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async loadQuoteModuleList({ commit }) {
            console.log('loadQuoteModuleList');
            //Déclaration variable
            let module_id = this.state.idModule;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-module'), {
                    idModule: module_id,
                })
                    .then((response) => {
                        commit("SET_QUOTEMODULE", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async loadInitComponentList({ commit }) {//IdComposants
            console.log('loadInitComponentList');
            //Déclaration variable
            let project_id = this.state.idProject;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-project-component'), {
                    idProject: project_id,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJETCOMPONENT", response.data);
                        resolve();
                    })
                    //.then((response) => {
                    //    commit("SET_QUOTEPROJETMODULE", response.data);
                    //    resolve();
                    //})
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async loadQuoteComponentList({ commit }) {//Envoi IDComposants recup composants
            console.log('loadQuoteComponentList*****');
            //Déclaration variable
            let component_id = this.state.idComponent;
            console.log('idComponent : ');
            console.log(component_id);

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/get-component'), {
                    idComponent: component_id,
                })
                    .then((response) => {
                        commit("SET_QUOTECOMPONENT", response.data);
                        resolve();
                    })
                    // .then((response) => {
                    //     commit("SET_QUOTEPROJETCOMPONENT", response.data);
                    //     resolve();
                    // })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async quoteSaveInformation({ commit }, payload) {
            //Déclaration variables
            let status = payload.status;
            let id = payload.id;
            let civ = payload.civ;
            let name = payload.name;
            let firstname = payload.firstname;
            let address = payload.address;
            let compaddress = payload.compaddress;
            let cp = payload.cp;
            let city = payload.city;
            let phonenumber = payload.phonenumber;
            let mail = payload.mail;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/save-informations-quote'), {
                    status: status,
                    cli_id: id,
                    cli_civ: civ,
                    cli_lastname: name,
                    cli_firstname: firstname,
                    cli_address: address,
                    cli_compaddress: compaddress,
                    cli_cp: cp,
                    cli_city: city,
                    cli_phonenumber: phonenumber,
                    cli_mail: mail,
                })
                    .then((response) => {
                        commit("SET_QUOTEINFORMATION", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async quoteSaveProject({ commit }, payload) {
            console.log('quoteSaveProject');
            //Déclaration variables
            let projectName = payload.project_name;
            let constructaddress = payload.building_address;
            let constructcompaddress = payload.building_comp_address;
            let constructcp = payload.building_postal_code;
            let constructcity = payload.building_city;
            let projectcreationdate = moment();
            let projectmodifydate = moment();
            let idUser = this.state.idUser;
            let idCustomer = this.state.idCustomer;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/save-options-quote'), {
                    name: projectName,
                    cli_construct_address: constructaddress,
                    cli_construct_comp_address: constructcompaddress,
                    cli_construct_cp: constructcp,
                    cli_construct_city: constructcity,
                    cli_project_creation_date: projectcreationdate,
                    cli_project_modify_date: projectmodifydate,
                    user: idUser,
                    customer: idCustomer,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJET", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        createPdf({ commit }) {
            console.log('createPdf');

            let customer = this.state.Customer;
            let project = this.state.Project;
            let login = this.state.login;
            let components = this.state.Component;
            let modules = this.state.Module;
            let products = this.state.Product;
            let ranges = this.state.rangeList;
            let models = this.state.modelList;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/create-pdf'), { customer, project, login, modules, products, components, ranges, models })
                    .then((response) => {
                        commit("SET_CREATEPDF", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    })
                    );
            });
        },
        async quoteSaveProjectProduct({ commit }, payload) {
            console.log('quoteSaveProjectProduct');
            console.log(payload);
            //Déclaration variables
            let productname = payload.product_name;
            let rangeselected = payload.range_selected;
            let modelselected = payload.model_selected;
            let principalcutselected = payload.principal_cut_selected;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/save-option-product'), {
                    nameProduct: productname,
                    selectedRange: rangeselected,
                    selectedModel: modelselected,
                    selectedPrincipalCut: principalcutselected,
                    projectId: this.state.idProject,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJETPRODUCT", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async quoteSaveProjectModule({ commit }, payload) {
            console.log('quoteSaveProjectModule');
            //Déclaration variables
            let modulename = payload.module_name;
            let modulelength = payload.module_length;
            let sectionselected = payload.section_selected;
            let cornerselected = payload.corner_selected;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/save-option-module'), {
                    name: modulename,
                    length: modulelength,
                    selected_section: sectionselected,
                    selected_corner: cornerselected,
                    projectId: this.state.idProject,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJETMODULE", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        async quoteSaveProjectComponent({ commit }, payload) {
            console.log('store - quoteSaveProjectComponent');
            //Déclaration variables
            let statutComponent = payload.status;
            let idComponent = payload.component_id;
            let componentname = payload.component_name;
            let productselected = payload.product_selected;
            let moduleselected = payload.module_selected;

            return new Promise((resolve, reject) => {
                axios.post((urlquote + '/save-option-component'), {
                    status: statutComponent,
                    id: idComponent,
                    nameComponent: componentname,
                    selectedProduct: productselected,
                    selectedModule: moduleselected,
                    projectId: this.state.idProject,
                })
                    .then((response) => {
                        commit("SET_QUOTEPROJETCOMPONENT", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        /*
        * PARTIE CONFIGURATION
        */
        loadModelList({ commit }) {
            console.log('loadModelList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/modele')
                    .then((response) => {
                        //console.log("loadModelList");
                        //console.log(response.data);
                        commit("SET_MODEL_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadRangeList({ commit }) {
            console.log('loadRangeList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/gamme')
                    .then((response) => {
                        console.log("loadRangeList");
                        console.log(response.data);
                        commit("SET_RANGE_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadFamilyComponentList({ commit }) {
            console.log('loadFamilyComponentList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/famille-composant')
                    .then((response) => {
                        //console.log("loadFamilyComponentList");
                        //console.log(response.data);
                        commit("SET_FAMILYCOMPONENT_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadComponentList({ commit }) {
            console.log('loadComponentList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/composant')
                    .then((response) => {
                        //console.log("loadComponentList");
                        //console.log(response.data);
                        commit("SET_COMPONENT_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadProviderList({ commit }) {
            console.log('loadProviderList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/fournisseur')
                    .then((response) => {
                        console.log("loadProviderList");
                        console.log(response.data);
                        commit("SET_PROVIDER_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadFrameList({ commit }) {
            console.log('loadFrameList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/huisserie')
                    .then((response) => {
                        //console.log("loadFrameList");
                        //console.log(response.data);
                        commit("SET_FRAME_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        loadSettingList({ commit }) {
            console.log('loadSettingList');
            return new Promise((resolve, reject) => {
                axios.get(urlconfiguration + '/parametres')
                    .then((response) => {
                        //console.log("loadSettingList");
                        //console.log(response.data);
                        commit("SET_SETTING_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        /*
        * PARTIE PAIEMENT
        */
        loadPaymentList({ commit }) {
            console.log('loadPaymentList');
            return new Promise((resolve, reject) => {
                axios.get(urlpayment)
                    .then((response) => {
                        //console.log("loadPaymentList");
                        //console.log(response.data);
                        commit("SET_PAYMENT_LIST", response.data);
                        resolve();
                    })
                    .catch((error => {
                        console.log(error.statusText, reject);
                    }))
            })
        },
        /*
        * PARTIE SYNTHESE
        */
        cptUserQuotesList({ commit }) {
            commit("SET_CPT_USER_QUOTE");
        },
        cptUserQuotesValidateList({ commit }) {
            commit("SET_CPT_USER_QUOTE_VALIDATE");
        },
        cptQuotesValidateList({ commit }) {
            commit("SET_CPT_QUOTE_VALIDATE");
        },
        cptUserQuotesRelaunchList({ commit }) {
            commit("SET_CPT_USER_QUOTE_RELAUNCH");
        },
    },
    modules: {}
})
