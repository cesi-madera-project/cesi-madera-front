import Vue from 'vue'
import vuex from 'vuex'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import axios from 'axios'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-search-select/dist/VueSearchSelect.css'
import VueMq from 'vue-mq'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(require('vue-moment'));

const token = localStorage.getItem('user-token')
if (token) {
  axios.defaults.headers.common['Authorization'] = token
}

Vue.use(VueMq,
  {
    breakpoints:
    {
      sm: 450,
      md: 1220,
      lg: Infinity,
    },
    defaultBreakpoint: 'sm'
  })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuex,
  axios,
  render: h => h(App)
}).$mount('#app')
