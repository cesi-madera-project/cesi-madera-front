import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store/index'
Vue.use(VueRouter)

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next()
        return
    }
    next('/')
}

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next()
        return
    }
    next('/login')
}

const routes = [{
    path: '/login',
    name: 'Connection',
    component: () =>
        import('../views/Connection.vue'),
    beforeEnter: ifNotAuthenticated,
},
{
    path: '/',
    name: 'Home',
    component: () =>
        import('../views/Home.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/devis',
    name: 'quotes',
    component: () =>
        import('../views/Quotes.vue'),
    props: {
        title: "Liste des devis",
        rechercheInput1: "Référence",
        rechercheInput2: "Nom      ",
        rechercheInput3: "Téléphone",
        rechercheInput4: "Email    ",
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/devis/information',
    name: 'new_quote',
    component: () =>
        import('../views/quotes/quoteInformation.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/devis/options',
    name: 'quotes_option',
    component: () =>
        import('../views/quotes/quoteOptions.vue'),
    beforeEnter: ifAuthenticated,
}, {
    path: '/devis/bilan',
    name: 'quote_bilan',
    component: () =>
        import('../views/quotes/quoteBilan.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/devis/:devis',
    name: 'edit_quote',
    component: () =>
        import('../views/quotes/quoteInformation.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration',
    name: 'Configuration',
    component: () =>
        import('../views/Configuration.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration/modeles',
    name: 'Modeles',
    component: () =>
        import('../views/configurationObject.vue'),
    props: {
        title: "Liste des modèles",
        rechercheInput1: "Nom",
        rechercheInput2: "Gamme",
        object: "model"
    },
    beforeEnter: ifAuthenticated,

},
{
    path: '/configuration/gammes',
    name: 'Gammes',
    component: () =>
        import('../views/configurationObject.vue'),
    props: {
        title: "Liste des gammes",
        rechercheInput1: "Nom",
        rechercheInput2: "",
        object: "range"
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration/famille-composants',
    name: 'FamilleComposants',
    component: () =>
        import('../views/configurationObject.vue'),
    props: {
        title: "Liste des familles de composants",
        rechercheInput1: "Nom",
        rechercheInput2: "",
        object: "family"
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration/composants',
    name: 'Composants',
    component: () =>
        import('../views/configurationObject.vue'),
    props: {
        title: "Liste des composants",
        rechercheInput1: "Nature",
        rechercheInput2: "Fournisseur",
        object: "component"
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration/fournisseurs',
    name: 'Fournisseurs',
    component: () =>
        import('../views/configurationObject.vue'),
    props: {
        title: "Liste des fournisseurs",
        rechercheInput1: "Nom",
        rechercheInput2: "",
        object: "provider"
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration/huisseries',
    name: 'Huisseries',
    component: () =>
        import('../views/configurationObject.vue'),
    props: {
        title: "Liste des huisseries",
        rechercheInput1: "Nom",
        rechercheInput2: "Gamme",
        object: "frame"
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/configuration/parametres',
    name: 'Parametres',
    component: () =>
        import('../views/configuration/parametres.vue'),
    props: {
        title: "Paramètres",
        rechercheInput1: "",
        rechercheInput2: "",
        object: "setting"
    },
    beforeEnter: ifAuthenticated,
},
{
    path: '/paiement',
    name: 'Payement',
    component: () =>
        import('../views/Payment.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/synthese',
    name: 'Dashboard',
    component: () =>
        import('../views/Dashboard.vue'),
    beforeEnter: ifAuthenticated,
},
{
    path: '/support',
    name: 'Support',
    component: () =>
        import('../views/Support.vue'),
    beforeEnter: ifAuthenticated,
},
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router